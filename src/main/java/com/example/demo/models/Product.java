package com.example.demo.models;

import lombok.Data;

@Data
public class Product {
	private int productId;
	private String name;
	private String availabale;
	private double price;
}
