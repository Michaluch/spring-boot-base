package com.example.demo.models;

public class UserModel {
	private String firstName;
	private String lastName;
	
	public UserModel(String fName, String lName) {
		this.setFirstName(fName);
		this.setLastName(lName);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
}
