package com.example.demo.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class BaseResponse {
	private  String status;
	private  Integer code;

	public BaseResponse(String status, int code) {
        this.setStatus(status);
        this.setCode(code);
    }

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

}
