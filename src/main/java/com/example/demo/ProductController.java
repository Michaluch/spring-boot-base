package com.example.demo;

import org.springframework.web.bind.annotation.*;

import com.example.demo.models.BaseResponse;
import com.example.demo.models.Product;

@RestController
@RequestMapping("/products")
public class ProductController {
	private final String sharedKey = "SHARED_KEY";

    private static final String SUCCESS_STATUS = "success";
    private static final String ERROR_STATUS = "error";
    private static final int CODE_SUCCESS = 100;
    private static final int AUTH_FAILURE = 102;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public BaseResponse showStatus() {
        return new BaseResponse(SUCCESS_STATUS, 2);
    }
    
       
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public BaseResponse pay(@RequestParam(value = "key") String key, @RequestBody Product request) {

        final BaseResponse response;

        if (sharedKey.equalsIgnoreCase(key)) {
            int productId = request.getProductId();
            String name = request.getName();
            String availabale = request.getAvailabale();
            double price = request.getPrice();
           
            response = new BaseResponse(SUCCESS_STATUS, CODE_SUCCESS);
        } else {
            response = new BaseResponse(ERROR_STATUS, AUTH_FAILURE);
        }
        return response;
    }
}
