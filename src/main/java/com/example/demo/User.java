package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.example.demo.models.UserModel;

@RestControllerAdvice
@RequestMapping("/api")
public class User {
	
	@RequestMapping("/user")
	public UserModel returnUser() {
		UserModel newUser = new UserModel("Oleksa", "Dovbush");
		newUser.setFirstName("Dmytro");
		
		return newUser;
	} 
}
